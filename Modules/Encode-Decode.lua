local invis1 = "\u{200B}"
local invis2 = "\u{FF01}"
function encrypt(str,key)
	math.randomseed(key)
	local newstr = ""
	for i = 1,#str do
		local sub = str:sub(i,i)


		local ret = key-string.byte(sub)
		if math.random(1,2) == 1 then
			ret = invis2..ret+key
		else
			ret = "!"..ret
		end
		if math.random(1,2) == 1 then
			ret = invis1..string.reverse(ret)
		end

		newstr = newstr.."."..ret
	end
	return newstr:sub(2)
end

function decrypt(str,key)
	local newstr = ""

	str = str:gsub("!","")

	local split = str:split(".")
	for _,v in pairs(split) do
		if v:find(invis1) then
			v = string.reverse(v:gsub(invis1,""))
		end
		local oldv = v

		v = v:gsub(invis2,"")

		local num = key-tonumber(v)

		if oldv:find(invis2) then
			num = num+key
		end

		newstr = newstr..string.char(num)
	end
	return newstr
end

local key = 1

local tab = setmetatable({version="1.1"},{
	__call = function(_,newkey)
		key = newkey
	end,
	__index = function(_,i)
		local li = i:lower()
		
		if li == "encrypt" then
			return function(a)
				return encrypt(a,key)		
			end
		elseif li == "decrypt" then
			return function(a)
				return decrypt(a,key)		
			end
		end
	end,
})

return tab

--[[local en = encrypt("print(\"Hello, World!\")",key)
print(en)
local de = decrypt(en,key)
print(de)--]]

--local library = loadstring(game:GetService("HttpService"):GetAsync("https://raw.githubusercontent.com/Wapplee/Lua-Sandbox-Scripts/main/Modules/Main.lua"))()

local path = "https://raw.githubusercontent.com/Wapplee/Lua-Sandbox-Scripts/main/Modules/"
function getsrc(pathadd,load)
    local ret = game:GetService("HttpService"):GetAsync(pathadd,true)
    if load then
        ret = loadstring(ret)()
    end
    return ret
end
function getlsrc(add,load)
    return getsrc(path..add..".lua",load)
end

local scriptsrc = getlsrc("Main")

return {
    version = "1.0.1",
    encoder = getlsrc("Encode-Decode",true),
    get = {
        lsrc = getlsrc,
        src = getsrc,
    },
}

